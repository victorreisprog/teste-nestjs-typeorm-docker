FROM node:20

# Criando diretório da aplicação
WORKDIR /usr/src/app

# Instalando as dependências da aplicação
COPY package*.json ./

RUN npm ci

# Copiando todo o resto da aplicação no diretório principal

COPY . .

RUN npm run build

EXPOSE 3000

CMD ["npm", "run", "start:prod"]