import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './users.entity';

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(User)
        private usersRepository: Repository<User>,
    ) {}

    async findAll(): Promise<User[]> {
        return await this.usersRepository.find()
    }

    async findOne(id: number): Promise<User> {
        return await this.usersRepository.findOne({ where: { id }})
    }
 
    async createUser(user: User): Promise<User> {
        const newUser = await this.usersRepository.create(user)
        return await this.usersRepository.save(newUser)
    }

    async updateUser(id: number, user: User) {
        const oldUser = await this.usersRepository.findOne({ where: { id }})

        if (!oldUser) {
            throw new NotFoundException(`Usuário com id - ${id} não foi encontrado`)
        }

        await this.usersRepository.update(id, user)
        return this.usersRepository.findOne({ where: { id }})
    }

    async updateUser2(id: number, updatedData: Partial<User>): Promise<User> {
        // o type Partial<User> vai indicar que nem todos os campos vão ser obrigatórios, só os campos que eu especificar que quero atualizar
        const user = await this.usersRepository.findOne({ where: { id }})

        if (!user) {
            throw new NotFoundException(`Usuário com id - ${id} não foi encontrado`)
        }

        await this.usersRepository.update(id, updatedData)

        return this.usersRepository.findOne({ where: { id }})
    }

    async delete(id: number): Promise<void> {

        const user = await this.usersRepository.findOne({ where: { id }})

        if (!user) {
            throw new NotFoundException(`Usuário com id - ${id} não foi encontrado`)
        }

        await this.usersRepository.delete(id)
    }

}
