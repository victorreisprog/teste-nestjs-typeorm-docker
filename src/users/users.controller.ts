import { Controller, Get, Post, Put, Delete, Body, Param, NotFoundException } from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from './users.entity';

@Controller('users')
export class UsersController {

    constructor(private readonly usersService: UsersService) {}

    @Get()
    async findAll(): Promise<User[]> {
        return await this.usersService.findAll()
    }

    @Get(':id')
    async findOne(@Param('id') id: number): Promise<User> {
        // param está pegando o id que está sendo enviado como parametro na url
        const user = await this.usersService.findOne(id)

        if (!user) {
            throw new NotFoundException('Usuário não encontrado')
        } else {
            return user
        }
    }

    @Post()
    async create(@Body() user: User): Promise<User> {
        return await this.usersService.createUser(user)
    }

    @Put(':id') 
    async update(@Param('id') id: number, @Body() user: User): Promise<User> {
        return await this.usersService.updateUser(id, user)
    }

    @Delete(':id')
    async delete(@Param('id') id: number): Promise<void> {
        const user = await this.usersService.findOne(id)

        if (!user) {
            throw new NotFoundException('Usuário não foi encontrado')
        }

        return this.usersService.delete(id)
    }
 
}
